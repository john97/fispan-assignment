import React, { useEffect, useState } from "react";
import {
  Container,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Toast,
  ToastBody,
  ToastHeader,
} from "reactstrap";

import RatesTable from "./components/RatesTable";

const host = "api.frankfurter.app";

function App() {
  const [medianRate, setMedianRate] = useState(null);
  const [latestRate, setLatestRate] = useState(null);
  const [buy, setBuy] = useState(false);
  const [modal, setModal] = useState(false);
  const [error, setError] = useState(false);

  const fromDate = 30;

  useEffect(() => {
    fetchLatestRate();
  }, [buy]);

  const fetchLatestRate = () => {
    fetch(`https://${host}/latest?amount=1&to=CAD,USD`)
      .then((resp) => resp.json())
      .then((data) => {
        const rate = data.rates.USD / data.rates.CAD;
        setLatestRate(rate);
        fetchHistoricalRates(fromDate, rate);
      })
      .catch((err) => {
        setError(true);
      });
  };

  const fetchHistoricalRates = (days, latestRate) => {
    // accepts parametres for the number of days to get historical data from and the latest CAD to USD rate
    const today = new Date();
    const priorDateTimestamp = new Date().setDate(today.getDate() - days);
    var priorDate = new Date(priorDateTimestamp);

    // format date to yyyy-mm-dd
    priorDate =
      priorDate.getFullYear() +
      "-" +
      ("0" + (priorDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + priorDate.getDate()).slice(-2);

    fetch(`https://${host}/${priorDate}..?to=CAD,USD`)
      .then((resp) => resp.json())
      .then((data) => {
        const ratesArray = Object.values(data.rates);
        const rates = ratesArray.map((rate, i) => {
          return rate.USD / rate.CAD;
        });

        // calculate the median rate from the historical dates given
        const medianR = median(rates);

        setMedianRate(medianR);

        if (latestRate > medianR) {
          // if the latest rate is better than the median rate, set buy state to true
          setBuy(true);
        }
      })
      .catch((err) => {
        setError(true);
      });
  };

  const median = (arr) => {
    const mid = Math.floor(arr.length / 2);
    const nums = [...arr].sort((a, b) => a - b);
    return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
  };

  const handleSubmit = () => {
    toggle();
  };

  const toggle = () => setModal(!modal);

  return (
    <>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">CAD to USD rates</NavbarBrand>
      </Navbar>
      <Container style={{ marginTop: "20px" }}>
        <Row>
          <Col>
            <p>
              It is best to buy <b>CAD</b> right now.
            </p>
          </Col>
        </Row>
        <RatesTable
          rates={{ latestRate, medianRate }}
          fromDate={fromDate}
          buy={buy}
          handleSubmit={handleSubmit}
        />
        {error && (
          <Toast className="bg-danger">
            <ToastHeader>Error</ToastHeader>
            <ToastBody style={{ color: "#fff" }}>
              An error occurred while fetching the data.
            </ToastBody>
          </Toast>
        )}
      </Container>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Operation complete</ModalHeader>
        <ModalBody>Successfuly {buy ? "bought" : "sold"} CAD.</ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
}

export default App;
