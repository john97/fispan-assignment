import { Table, Button } from "reactstrap";

const RatesTable = ({ rates, fromDate, buy, handleSubmit }) => {
  return (
    <Table>
      <thead>
        <tr>
          <th>Latest rate</th>
          <th>Median rate (past {fromDate} days)</th>
          <th>Operation</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{rates.latestRate}</td>
          <td>{rates.medianRate}</td>
          <td>
            <Button color="success" block onClick={() => handleSubmit()}>
              {buy ? "Buy CAD!" : "Sell CAD"}
            </Button>
          </td>
        </tr>
      </tbody>
    </Table>
  );
};

export default RatesTable;
